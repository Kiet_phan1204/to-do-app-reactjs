import React, { Component } from 'react';
import Title from './components/Title';
import Control from './components/Control';
import Form from './components/Form';
import List from './components/List';

import tasks from './mocks/tasks';
const _ = require('lodash');
const uuidv4 = require('uuid/v4');


class App extends Component {
    constructor(props) {
        super(props);
        this.state = {
            items: tasks,
            isShowForm: false,
            strSearch: '',
            orderBy: 'name',
            order: 'ASC',
            selectedItem: {}
        };
    }

    handleToggleForm = () => {
        this.setState({
            isShowForm: !this.state.isShowForm
        });
    }

    handleCancel = () => {
        this.setState({
            isShowForm: false
        });
    }

    handleSearch = (value) => {
        this.setState({
            strSearch: value
        });
    }

    handleSort = (orderBy, order) => {
        let items = _.orderBy(this.state.items, [orderBy], [order]);
        this.setState({
            items: items,
            orderBy: orderBy,
            order: order
        });
    }

    handleDelete = (id) => {
        let items = this.state.items.filter((item) => {
            return item.id !== id;
        });
        this.setState({
            items : items
        });
    }

    handleSubmit = (item) => {
        console.log(item);
        let {items} = this.state;
        if (item.id === undefined) {
            items.push({
                id: uuidv4(), 
                name: item.name,
                evel: parseInt(item.level)
            });
        } else {
            let items = items.map((task, index) => {
                if (task.id === item.id) {
                    items[index] = item.name;
                    items[index] = item.level;
                }
            });
            console.log(items);
        }
        
        this.setState({
            items,
            isShowForm: false
        });
    }

    handleEdit = (item) => {
        this.setState({
            selectedItem: item,
            isShowForm: true
        });
    }

    render() {
        let itemsOrigin = this.state.items;
        let items = itemsOrigin.filter((item) => {
            return item.name.toLowerCase().indexOf(this.state.strSearch) > -1;
        });
        let {order, orderBy, selectedItem } = this.state;

        let elmtForm = null;
        if (this.state.isShowForm === true) {
            elmtForm = <Form onClickEdit={this.handleEdit} onClickSubmit={this.handleSubmit} onClickCancel={this.handleCancel} selectedItem={this.state.selectedItem} />;
        }

        return (
          <div className="row">
            {/* TITLE START */}
            <Title />
            {/* TITLE END */}

            {/* CONTROL (SEARCH - SORT - ADD) : START */}
            <Control
                onClickSearchGo={this.handleSearch}
                onClickAdd={this.handleToggleForm}
                isShowForm={this.state.isShowForm}
                onClickSort={this.handleSort}
                orderBy={orderBy}
                order={order}
            />
            {/* CONTROL (SEARCH - SORT - ADD) : END */}

            {/* FORM START */}
            {elmtForm}
            {/* FORM END */}

            {/* LIST START */}
            <List onClickEdit={this.handleEdit} onClickDelete={this.handleDelete} items={items} />
            {/* LIST END */}
          </div>
        );
    }
}

export default App;
