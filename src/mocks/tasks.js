const uuidv4 = require('uuid/v4');

let items = [
    {
        id: uuidv4(),
        name: "ABC Lorem ipsum sit amet, consectetur adipisicing elit.",
        level: 0
    },
    {
        id: uuidv4(),
        name: "DEF Lorem ipsum dolor sit amet, consectetur adipisicing elit.",
        level: 1
    },
    {
        id: uuidv4(),
        name: "GHI Lorem ipsum sit amet, consectetur adipisicing elit.",
        level: 2
    },
    {
        id: uuidv4(),
        name: "JKL Lorem ipsum sit amet, consectetur adipisicing elit.",
        level: 0
    },
    {
        id: uuidv4(),
        name: "MNO Lorem ipsum dolor sit amet, consectetur adipisicing elit.",
        level: 0
    },
    {
        id: uuidv4(),
        name: "VXT Lorem ipsum dolor sit amet, consectetur adipisicing elit.",
        level: 2
    },
    {
        id: uuidv4(),
        name: "XYZ Lorem ipsum sit amet, consectetur adipisicing elit.",
        level: 1
    },
    {
        id: uuidv4(),
        name: "ABY Lorem ipsum dolor sit amet, consectetur adipisicing elit.",
        level: 0
    }
];

export default items;