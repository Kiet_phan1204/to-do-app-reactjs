import React, { Component } from 'react';

class Form extends Component {
  constructor(props) {
    super(props);
    this.state = {
      task_id: '',
      task_name: '',
      task_level: 0
    };
  }

  componentWillMount() {
    let item = this.props.selectedItem;
    if (item.id !== '') {
      this.setState({
        task_id: item.id,
        task_name: item.name,
        task_level: item.level
      });
    }
  }

  handleCancel = () => {
    this.props.onClickCancel();
  }

  handleChange = (event) => {
    const target = event.target;
    const value = target.type === 'checkbox' ? target.checked : target.value;
    const name = target.name
    this.setState({
        [name]: value
    });
  }

  handleSubmit = (event) => {
    let item = {
      id: this.state.id,
      name: this.state.task_name,
      level: this.state.task_level
    };
    this.props.onClickSubmit(item);
    event.preventDefault();
  }

  render() {
    // let item = this.props.selectedItem;
    // if (item.id !== '') {
    //   this.setState({
    //     task_id: item.id,
    //     task_name: item.name,
    //     task_level: item.level
    //   });
    // }
    return (
      <div className="row">
        <div className="col-md-offset-7 col-md-5">
            <form onSubmit={this.handleSubmit} className="form-inline">

                <div className="form-group">
                    <label className="sr-only" htmlFor="true">Name</label>
                    <input name="task_name" onChange={this.handleChange} value={this.state.task_name} type="text" className="form-control" placeholder="Task Name" />
                </div>

                <div className="form-group">
                    <label className="sr-only" htmlFor="true">Level</label>
                    <select name="task_level" value={this.state.task_level} onChange={this.handleChange} className="form-control" >
                        <option defaultValue={parseInt(this.state.task_level) === 0} value="0">Small</option>
                        <option defaultValue={parseInt(this.state.task_level) === 1} value="1">Medium</option>
                        <option defaultValue={parseInt(this.state.task_level) === 2} value="2">High</option>
                    </select>
                </div>

                <button type="submit" className="btn btn-primary">Submit</button>
                <button onClick={this.handleCancel} type="button" className="btn btn-default">Cancel</button>
            </form>
        </div>
      </div>
    );
  }
}

export default Form;
