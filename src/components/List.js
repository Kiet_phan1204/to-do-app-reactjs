import React, { Component } from 'react';
import Item from './Item';

class List extends Component {
  render() {
    const items = this.props.items;
    const elmtItems = items.map((item, index) => {
        return (
            <Item onClickDelete={this.props.onClickDelete} onClickEdit={this.props.onClickEdit} key={index} item={item} index={index} />
        )
    });
    return (
      <div className="panel panel-success">
        <div className="panel-heading">List task</div>
        <table className="table table-hover">
            <thead>
                <tr>
                    <th style={{width: '10%'}} className="text-center">#</th>
                    <th>Task</th>
                    <th style={{width: '20%'}} className="text-center">Level</th>
                    <th style={{width: '20%'}}>Action</th>
                </tr>
            </thead>
            <tbody>
                {elmtItems}
            </tbody>
        </table>
      </div>
    );
  }
}

export default List;
