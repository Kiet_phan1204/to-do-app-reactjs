import React, { Component } from 'react';

class Item extends Component {
    /*
    1: default
    2: info
    3: danger
     */
    showElementLevel(level) {
        let elmtLevel = <span className="label label-default">Small</span>;
        if (level === 1) {
            elmtLevel = <span className="label label-info">Medium</span>;
        } else if (level === 2) {
            elmtLevel = <span className="label label-danger">High</span>;
        }
        return elmtLevel;
    }

    handleEdit = (item) => {
        this.props.onClickEdit(item);
    }

    handleDelete = (id) => {
        this.props.onClickDelete(id);
    }

    render() {
        const {item, index} = this.props;
        return (
            <tr>
                <td className="text-center">{index + 1}</td>
                <td>{item.name}</td>
                <td className="text-center">{this.showElementLevel(item.level)}</td>
                <td>
                    <button onClick={() => this.handleEdit(item)} type="button" className="btn btn-warning">Edit</button>
                    <button onClick={() => {this.handleDelete(item.id)}} type="button" className="btn btn-danger">Delete</button>
                </td>
            </tr>
        );
    }
}

export default Item;
